package UtilPackage;

/**
 * Created by Grevling on 12/01/2017.
 */
public abstract class StaticValueArchive {
    
    public static String PATH_START = "Path Start";
    public static String PATH_END = "Path End";

	public static int MIN_CYCLE_LENGTH = 3;
	public static int MAX_CYCLE_LENGTH = 6;
	
    public static long MIN_BELIEVABLE_REGION_STAY_DURATION = 30000; // 30000 MS = 30 SEC
    public static long MAX_BELIEVABLE_REGION_STAY_DURATION = 1800000; // 1800000 MS = 30 MIN
    public static long MAX_BELIEVABLE_TRAVEL_TIME = 300000; // 300000 MS = 5 MIN

}
