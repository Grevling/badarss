package UtilPackage;

import org.json.JSONArray;
import org.json.JSONObject;
import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public abstract class Utils {
    private static DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

    /**
     * Convert a string in format "yyyy-MM-dd HH:mm:ss" into a Date object
     * @param dateString String in format "yyyy-MM-dd HH:mm:ss"
     * @return Date object based on input string
     */
    public static Date dateFromString(String dateString){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date date = null;
        try {
            date = df.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Long averageLongFromList(List<Long> longList) {
        Long sum = new Long(0);
        int numberOfValuesRegistered = 0;

        for (Long l : longList) {
            if (l > 0) {
                sum += l;
                numberOfValuesRegistered ++;
            }
        }

        if (numberOfValuesRegistered > 0) {
            return sum / numberOfValuesRegistered;
        } else { return new Long(0); }
    }

    /**
     * Takes two dates and returns the earliest one. If one date is null, the other is returned.
     * @param x Date to be comared
     * @param y Date to be comared
     * @return the earliest of the two date arguments
     */
    public static Date getFirstDate(Date x, Date y) {
        if (x == null) { return y; }
        if (y == null) { return x; }
        if (x.compareTo(y) < 0) {  // if x is before y
            return x;
        } else {
            return y;
        }
    }

    public static String constructIdStringFromStringArrayList(ArrayList<String> stringArrayList) {
        StringBuilder sb = new StringBuilder("");
        for (String s : stringArrayList) {
            if (!sb.toString().equals("")) {
                sb.append("-");
            }
            sb.append(s);
        }
        return sb.toString();
    }

    public static Long lowestLong(Long long1, Long long2) {
        if (long1 > long2) return long2;
        return long1;
    }

    /**
     * Converts a long (interpreted as milliseconds) to a time string of format HH:mm:ss
     * @param millis long
     * @return time string in format HH:mm:ss
     */
    public static String millisToString(Long millis) {
        return String.format("%d:%d:%d",
                TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis),
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
        );
    }

    /**
     * Writes a string to a file as specified
     * @param content the string that is written to the file
     * @param fileName the path of the file, including file name and extension
     */
    public static void writeStringToFile(String content, String fileName) {
        try (PrintWriter out = new PrintWriter(fileName)) {
            out.println(content);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Reads a file from disk and returns content as string
     * @param file the path of the file, including file name and extension
     * @return String -  the contents of the file
     * @throws IOException
     */
    public static String readFile(String file) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String         line = null;
        StringBuilder  stringBuilder = new StringBuilder();
        String         ls = System.getProperty("line.separator");

        try {
            while((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }

            return stringBuilder.toString();
        } finally {
            reader.close();
        }
    }

    /**
     * Parse a Date object to String in format "yyyy-MM-dd HH:mm:ss"
     * @param date Date object that should be parsed to String
     * @return String in format "yyyy-MM-dd HH:mm:ss"
     */
    public static String dateToString(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        return df.format(date);
    }

    public static Date stringToDate(String dateString) {
        try {
            return df.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Get the difference between two dates in milliseconds.
     * Return value is positive if the first argument is chronologically before the second, negative if the opposite is true.
     * @param firstDate Date
     * @param secondDate Date
     * @return long millisecond difference between the two Date parameters
     */
    public static long getTimeDifMillis(Date firstDate, Date secondDate) {
        return (secondDate.getTime() - firstDate.getTime());
    }

    /**
     * Removes any negative value from a long
     * @param l long
     * @return positive long
     */
    public static long zeroOrHigher(long l) {
        if (l < 0) {
            return 0;
        } else {
            return l;
        }
    }

    /**
     * Converts a json string to a json object and extracts a json array containing json objects, each representing an
     * enter- or exit event.
     * @param jsonString a string of valid json where the root object contains an array called 'events'
     * @return JSONArray containing event objects.
     */
    public static JSONArray stringToJsonArray(String jsonString) {
        JSONObject job = new JSONObject(jsonString);
        JSONArray jArray = (JSONArray) job.get("events");
        return jArray;
    }



}
