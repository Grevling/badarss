package Controllers;

import Models.EdgeModel;
import Models.EventModels.EventModelContainer;
import Models.NodeModel;
import UtilPackage.Utils;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;

public class Controller {
    public Controller() {
        // get the event models
        EventModelContainer emContainer = FireBaseCommunicado.getEvents();

        // give event models to a new pathfinder
        PathController pathFinder = new PathController(emContainer.getEntryEventsPerSubject(), emContainer.getExitEventsPerSubject());

        // get node- and edge models from pathfinder
        ArrayList<NodeModel> nodes = pathFinder.getNodes();
        ArrayList<EdgeModel> edges = pathFinder.getEdges();

        // generate graph json string from node- and edge models
        String s = JsonFactory.getJson(nodes, edges);

        // save graph json where the javascript can find it
        Utils.writeStringToFile(s, "graph_html/fakeGraph.json");

        // print success message
        System.out.println("SUCCESS! " + Utils.millisToString(new Long (653000)));
    }
}