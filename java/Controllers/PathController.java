package Controllers;

import Models.EdgeModel;
import Models.EventModels.EventModel;
import Models.NodeModel;
import Models.verionTwoModels.RegionVisitModel;
import UtilPackage.Utils;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import static UtilPackage.StaticValueArchive.*;
import static UtilPackage.Utils.lowestLong;

public class PathController {
    private Map<String, Long> regionTimeSpentMap;
    private ArrayList<EventModel> entryList;
    private ArrayList<EventModel> exitList;
    private RegionVisitModel firstRegionVisitModel;
    private ArrayList<RegionVisitModel> regionVisitModelChains;
    private Map<String, Long> regionTimeMap;
    private Map<String, EdgeModel> correlationEdgeMap;
    private Map<String, EdgeModel> edgeMap;
    private Map<String, NodeModel> nodeMap;
    private boolean first;

    public PathController(HashMap<String, ArrayList<EventModel>> entryEventMap, HashMap<String, ArrayList<EventModel>> exitEventMap) {
        // ITERATE THROUGH SUBJECTS
        edgeMap = new HashMap<>();
        nodeMap = new HashMap<>();
        for (String key : entryEventMap.keySet()) {
            entryList = entryEventMap.get(key);
            instantiateFields();
            if (exitEventMap.keySet().contains(key)) { exitList = exitEventMap.get(key); }
            findPath();
        }
        calculateMarkovValuesInGraph();
    }

    public ArrayList<NodeModel> getNodes() { return new ArrayList<>(nodeMap.values()); }
    public ArrayList<EdgeModel> getEdges() { return new ArrayList<>(edgeMap.values()); }

    private void instantiateFields() {
        regionVisitModelChains = new ArrayList<>();
        regionTimeMap = new HashMap();
        exitList = new ArrayList<>();
        correlationEdgeMap = new HashMap<>();
        regionTimeSpentMap = new HashMap<>();
    }

    private void findPath() {
        first = true;
        linkEntryEvents();
        assembleRegionRawVisitModels();
        performRegionVisitModelLingerTimeSanityCheck();
        setAverageLingerValuesToUnrealisticRegionVisitModels();
        mapRegionLingerTimes();
        insertPathEndsAndPathStartsWhereAppropriate();
        generateAndOrUpdateNodesAndEdgesFromRegionVisitModels();
        findAndMarkCycles();
        calculateRegionLingerTimeCorrelations();
    }

    //PRIMARY PATHFINDING METHODS

    private void linkEntryEvents() {
        EventModel prevEvent = null;
        for (EventModel e : entryList) {
            if (first && !e.getRegion().equals("Region1")) {
            }
            first = false;
            if (prevEvent != null) {
                prevEvent.setNextEvent(e);
            }
            prevEvent = e;
        }
    }

    /**
     * Assembles a linked 'list' of RegionVisitModels without overlapping times, but without filtering for unreasonably long
     * durations for region visits or travel between regions.
     */
    private void assembleRegionRawVisitModels() {
        firstRegionVisitModel = new RegionVisitModel(PATH_START); // create a path start RegionVisitModel (RVM) as head of the linked 'list'
        regionVisitModelChains.add(firstRegionVisitModel);
        RegionVisitModel previousRegionVisit = firstRegionVisitModel; // set that head as the previously visited region for later convenience
        for (EventModel eem : entryList) {                       // iterate through the list of EntryEvents
            String regionName = eem.getRegion();
            Date entryTime = eem.getTime();
            Date exitTime = entryTime;                                                              // set exitTime to entryTime to guard against nulls
            Date exitEventTime = getNextExitEventTimeByTimeAndRegion(entryTime, regionName);        // look for exit events for this region after the time (Date) of this entry event
            if (eem.getNextEvent() != null){                                                        // if an applicable exit event was found...
                exitTime = Utils.getFirstDate(eem.getNextEvent().getTime(), exitEventTime);         // ...set exitTime to the time of the next exitEvent or the next entryEvent, whichever is first (chronologically in terms of Date objects) and not null
            } else if (exitEventTime != null)  {                                                    // if there is no next event but we did find an applicable exit time...
                exitTime = exitEventTime; }                                                         // ...use that one
            previousRegionVisit.setNextRegionVisited(new RegionVisitModel(regionName, entryTime, exitTime, previousRegionVisit));  // create the new RVM and put it in the previous RVM's nextRegionVisited field.
            previousRegionVisit = previousRegionVisit.getNextRegionVisited();                   // the new RVM is now the previous one.
        }
        previousRegionVisit.setNextRegionVisited(new RegionVisitModel(PATH_END));               // assign a new path end RVM as the last 'real' RVM's nextRegionVisited field, this is the tail of the linked 'list'
    }

    /**
     * performs a 'sanity check' on the RegionVisitModels in terms of the time spent in a region: if someone seems to have stayed in
     * a region longer than the threshold set the system assumes that an exit event has been missed for some reason. Such models are tagged
     * to have their time set to an average value.
     */
    private void performRegionVisitModelLingerTimeSanityCheck() {
        RegionVisitModel rvm = firstRegionVisitModel;
        while (rvm.hasNext()) {
            if (!rvm.isPathStartOrEnd()) {
                Long lingerTime = rvm.getExitTime().getTime() - rvm.getEnterTime().getTime();
                if (lingerTime > MAX_BELIEVABLE_REGION_STAY_DURATION) {
                    rvm.markAsUnreasonablyLengthy();
                } else if (lingerTime < MIN_BELIEVABLE_REGION_STAY_DURATION) {
                    rvm.markAsNotRealVisit();
                }
                if (rvm.getRegionName().equals(rvm.getPreviousRegionVisited().getRegionName())) {
                    rvm.markAsRepeatVisit();
                }
            }
            rvm = rvm.getNextRegionVisited();
        }
    }

    /**
     * finds the average time spent in regions excluding those tagged as unrealistically lengthy. those tagged models are set to the average
     */
    private void setAverageLingerValuesToUnrealisticRegionVisitModels() {
        RegionVisitModel rvm = firstRegionVisitModel;
        ArrayList<Long> timeValues = new ArrayList<>();

        while (rvm.hasNext()) {
            if (!rvm.isPathStartOrEnd() && !rvm.isUnreasonablyLengthy()) {
                Long lingerTime = rvm.getExitTime().getTime() - rvm.getEnterTime().getTime();
                if (lingerTime > 0) {
                    timeValues.add(lingerTime);
                }
            }
            rvm = rvm.getNextRegionVisited();
        }
        Long averageTimeValue = Utils.averageLongFromList(timeValues);
        rvm = firstRegionVisitModel;
        while (rvm.hasNext()) {
            if (rvm.isUnreasonablyLengthy()) {
                rvm.setExitTime(new Date(rvm.getEnterTime().getTime() + averageTimeValue));
            }
            rvm = rvm.getNextRegionVisited();
        }
    }

    private void mapRegionLingerTimes() {
        RegionVisitModel rvm = firstRegionVisitModel.getNextRegionVisited();
        while (rvm.hasNext() && !rvm.getRegionName().equals(PATH_START)) {
            mapRegionLingerTime(rvm.getRegionName(), rvm.getDuration());
            rvm = rvm.getNextRegionVisited();
        }
    }

    private void mapRegionLingerTime(String regionName, Long time) {
        if (time > 0) {
            if (regionTimeMap.keySet().contains(regionName)) {
                regionTimeMap.put(regionName, regionTimeMap.get(regionName) + time);
            } else {
                regionTimeMap.put(regionName, time);
            }
        }
    }

    /**
     * Splits the chain of RegionVisitModels of one user into separate chains representing visits to the store. Each chain starts with an
     * 'artificial' node representing the start of a path, and ends with an artificial end node (artificial here means that the node is not made from en entry event (beacon data))
     */
    private void insertPathEndsAndPathStartsWhereAppropriate() {
        RegionVisitModel rvm = firstRegionVisitModel;
        Long highestTravelTime = new Long(0);
        while (rvm.hasNext()) {
            if (!rvm.getRegionName().equals(PATH_START) && !rvm.getNextRegionVisited().getRegionName().equals(PATH_END)) {
                Long travelTimeToNextRegion = rvm.getNextRegionVisited().getEnterTime().getTime() - rvm.getExitTime().getTime();
                if (travelTimeToNextRegion > MAX_BELIEVABLE_TRAVEL_TIME) {
                    // save a reference to the RVM about to be severed
                    RegionVisitModel targetRegionVisitModel = rvm.getNextRegionVisited();
                    // create an end to one path and a start to another
                    RegionVisitModel newEnd = new RegionVisitModel(PATH_END);
                    RegionVisitModel newStart = new RegionVisitModel(PATH_START);
                    // connect the end node to the tail of the old path and set the new start as the head of the new.
                    linkRvms(rvm, newEnd);
                    linkRvms(newStart, targetRegionVisitModel);
                    // add the new chain to the list
                    regionVisitModelChains.add(newStart);
                    rvm = newStart.getNextRegionVisited();
                } else {
                    rvm = moveOnOrPruneNext(rvm);
                }
            } else {
                rvm = rvm.getNextRegionVisited();
            }
        }
    }

    private RegionVisitModel moveOnOrPruneNext(RegionVisitModel rvm) {
        if (rvm.getNextRegionVisited().isRepeatVisit()) { // if the next visit is a repeat (visits the same region as this visit (rvm))
            if (!rvm.getNextRegionVisited().getRegionName().equals(rvm.getRegionName())) {
        }
            linkRvms(rvm, rvm.getNextRegionVisited().getNextRegionVisited()); // amputate the next visit and let the one after that take its place( A -> B -> C... becomes A -> C... )
            return rvm; // return the same rvm, we need to calculate again taking these changes into account.
        } else {
            if (rvm.getNextRegionVisited().getRegionName().equals(rvm.getRegionName())) {
            }
            return rvm.getNextRegionVisited();
        }
    }

    private void linkRvms(RegionVisitModel previous, RegionVisitModel next){
        previous.setNextRegionVisited(next);
        next.setPreviousRegionVisited(previous);
    }

    private void generateAndOrUpdateNodesAndEdgesFromRegionVisitModels() {
        for (RegionVisitModel r : regionVisitModelChains) { // r represents the start of a path (a visit to the store, represented by a chain of RegionVisitModels)
            RegionVisitModel thisRvm = r;
            while (thisRvm.hasNext()) {
                RegionVisitModel nextRegion = thisRvm.getNextRegionVisited();
                EdgeModel em = getEdgeModel(thisRvm.getRegionName(), nextRegion.getRegionName(), false); //get or create the relevant edgemodel
                if (em != null){
                    em.addWeight(); //add weight to the edgemodel signifying it has been traversed
                    if (thisRvm.isRealVisit()) {
                        if (nextRegion.isRealVisit()) { // if this was a real region visit (not just passing through):
                            em.addRealVisitWeight();
                        } else {
                            String targetRegionName = getFirstRealVisit(nextRegion).getRegionName(); // find next real visit
                            if (!thisRvm.getRegionName().equals(targetRegionName) && !targetRegionName.equals(PATH_END)) { // if that visit is to a different region than this region
                                EdgeModel realVisitEdge = getEdgeModel(thisRvm.getRegionName(), targetRegionName, false); // get relevant edge
                                realVisitEdge.addRealVisitWeight();
                            }
                        }
                    }
                    if(!thisRvm.getRegionName().equals(PATH_START)) {
                        em.getSource().addTime(thisRvm.getDuration()); // get the duration from the regionVisitModel and add it to the nodeModel
                    }
                }


                thisRvm = nextRegion;
            }
        }
    }

    private RegionVisitModel getFirstRealVisit(RegionVisitModel regionVisitModel) {
        if (regionVisitModel.isRealVisit() || regionVisitModel.isPathStartOrEnd()) {
            return regionVisitModel;
        } else {
            return getFirstRealVisit(regionVisitModel.getNextRegionVisited());
        }
    }

    /**
     * Takes two strings representing the source- and target region of a step and returns the appropriate edge model.
     * If the model already exists in the map it is retrieved and returned, if not one is created, added to the map and then returned.
     * This way each step model related to a specific edge can update that edge's weight and cycles
     * @param source String name of source region
     * @param target String name of target region
     * @return EdgeModel
     */
    private EdgeModel getEdgeModel(String source, String target, boolean traversed) {

        if (source.equals(PATH_START) && target.equals(PATH_END)) {
            return null;
        }
        if (source.equals(target)) {
            return null;
        }
        String key = source + ":" + target;
        if (edgeMap.keySet().contains(key)) {
            return edgeMap.get(key);
        } else {
            NodeModel sourceNode = getNodeModel(source);
            NodeModel targetNode = getNodeModel(target);
            int defaultVisible = 1;
            if(!traversed){defaultVisible = 0;}
            EdgeModel em = new EdgeModel(getNodeModel(source), getNodeModel(target), defaultVisible);
            edgeMap.put(key, em);
            return em;
        }
    }

    private NodeModel getNodeModel(String regionName) {
        if (nodeMap.keySet().contains(regionName)) {
            return nodeMap.get(regionName);
        } else {
            NodeModel newNode = new NodeModel(regionName);
            nodeMap.put(regionName, newNode);
            return newNode;
        }
    }

    private void findAndMarkCycles() {
        for (RegionVisitModel r : regionVisitModelChains) { // r represents the start of a path (a visit to the store, represented by a chain of RegionVisitModels)
            RegionVisitModel rvm = r;
            HashMap<String, RegionVisitModel> previousRegionVisitsMap = new HashMap<>(); // keeps track of which regions we have already visited and references to the relevant RVMs
            while (rvm.hasNext()) {
                String regionName = rvm.getRegionName();
                if (previousRegionVisitsMap.keySet().contains(rvm.getRegionName())) {
                    registerCycleIfValid(previousRegionVisitsMap.get(regionName));
                }
                previousRegionVisitsMap.put(regionName, rvm);
                rvm = rvm.getNextRegionVisited();
            }
        }
    }

    private void registerCycleIfValid(RegionVisitModel rvm) {
        registerCycleIfValid(rvm, rvm.getRegionName(), 1, new ArrayList<String>());
    }

    private void registerCycleIfValid(RegionVisitModel rvm, String targetRegionName, int iterationNumber, ArrayList<String> cycleRegionNames) {
        if (targetRegionName.equals(PATH_END) || targetRegionName.equals(PATH_START)) {
            return;
        }
        if (iterationNumber > MAX_CYCLE_LENGTH) { return; } // the complete cycle has been found but is too long to be interesting
        cycleRegionNames.add(rvm.getRegionName());
        if (rvm.getNextRegionVisited().getRegionName().equals(targetRegionName)) { // this means we've looked at all the regions in the cycle
            if (iterationNumber < MIN_CYCLE_LENGTH) {
				return;
            } else {
                cycleRegionNames.add(targetRegionName); // add the target region name so we include the final edge that completes the cycle
                markCycle(cycleRegionNames);
            }
        } else {
            registerCycleIfValid(rvm.getNextRegionVisited(), targetRegionName, iterationNumber + 1, cycleRegionNames);
        }
    }

    private void markCycle(ArrayList<String> cycleRegionNames) {
        String cycleId = Utils.constructIdStringFromStringArrayList(cycleRegionNames);
        for(int i = 0; i < cycleRegionNames.size() - 1; i++) {
            EdgeModel em = getEdgeModel(cycleRegionNames.get(i), cycleRegionNames.get(i + 1), false);
            em.addCycleTag(cycleId);
        }
    }

    private void calculateRegionLingerTimeCorrelations() {
        ArrayList<String> relevantRegionNameList = new ArrayList(regionTimeMap.keySet());
        int numberOfRelevantRegions = relevantRegionNameList.size();
        for (int i = 0; i < numberOfRelevantRegions - 1; i++) {//iterate through the indexes of regions, subtract 1 in the condition because we don't need to process the final region (it will be already have been compared to every other region)
            String r1 = relevantRegionNameList.get(i);
            Long time1 = regionTimeMap.get(r1);
            for(int j = i + 1; j < numberOfRelevantRegions; j++) {//iterate through the indexes of subsequent regions (including the final one)
                String r2 = relevantRegionNameList.get(j);
                Long time2 = regionTimeMap.get(r2);
                Long timeCorrelation = lowestLong(time1, time2);
                if (timeCorrelation > 0) {
                    EdgeModel em = getCorrelationEdge(r1, r2);
                    em.addTimeCorrelation(timeCorrelation);
                }
            }
        }
    }

    private EdgeModel getCorrelationEdge(String r1, String r2) {
        String key = r1 + ":" + r2;
        if (correlationEdgeMap.keySet().contains(key)) {
            return correlationEdgeMap.get(key);
        }else{
            key = r2 + ":" + r1;
            if (correlationEdgeMap.keySet().contains(key)) {
                return correlationEdgeMap.get(key);
            } else {
                EdgeModel em = getEdgeModel(r1, r2, false);
                em.markAsCorrelationEdge();
                correlationEdgeMap.put(key, em);
                return em;
            }
        }
    }

    public void calculateMarkovValuesInGraph() {
        for (NodeModel n : nodeMap.values()) {
            double edgeWeightSum = 0;
            for (EdgeModel e : n.getOutgoingEdgeList()) {
                edgeWeightSum += e.getWeight();
            }
            for (EdgeModel e : n.getOutgoingEdgeList()) {
                double markovValue =  e.getWeight() / edgeWeightSum;
                if (!Double.isNaN(markovValue)) {
                    e.setMarkovWweight(markovValue);
                }
            }
        }
    }

    public Date getNextExitEventTimeByTimeAndRegion(Date enterTime, String regionName) {
        for (EventModel m : exitList) {
            if (enterTime.compareTo(m.getTime()) < 0 && m.getRegion().equals(regionName)) {
                return m.getTime();
            }
        }
        return null;

    }


}
