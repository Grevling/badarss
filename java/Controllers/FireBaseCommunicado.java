package Controllers;

import Models.EventModels.EventModel;
import Models.EventModels.EventModelContainer;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;

public abstract class FireBaseCommunicado {
	private static final String baseUrl = "https://fbmasterbeaconproject.firebaseio.com/eventdata.json";

	public static EventModelContainer getEvents() {
		HashMap<String, ArrayList<EventModel>> entryMap = new HashMap<>();
		HashMap<String, ArrayList<EventModel>> exitMap = new HashMap<>();

		JSONObject testRootObject = new JSONObject(getJsonString()); //get json object containing all events
		Iterator subTestIter = testRootObject.keys();
		while (subTestIter.hasNext()) {
			JSONObject subTestObject = testRootObject.getJSONObject((String) subTestIter.next());
			Iterator userIdIter = subTestObject.keys();
			while (userIdIter.hasNext()) {
				String userId = (String) userIdIter.next();
				JSONObject userObject = subTestObject.getJSONObject(userId);
				ArrayList<EventModel> entryList = new ArrayList<>();
				ArrayList<EventModel> exitList = new ArrayList<>();
				if (entryMap.keySet().contains(userId)) {
					entryList = entryMap.get(userId);
				} else {
					entryMap.put(userId, entryList);
				}
				if (exitMap.keySet().contains(userId)) {
					exitList = exitMap.get(userId);
				} else {
					exitMap.put(userId, exitList);
				}

				eventModelListFromJsonObject(userObject, entryList, userId, true);
				eventModelListFromJsonObject(userObject, exitList, userId, false);
			}

		}
		return new EventModelContainer(entryMap, exitMap);
	}

	private static void eventModelListFromJsonObject(JSONObject jsonObject, ArrayList<EventModel> eventList,  String subjectId,  boolean entry) {
		Iterator iter = jsonObject.keys();
		while (iter.hasNext()) {
			String key = (String) iter.next();
			JSONObject job = jsonObject.getJSONObject(key);
			if (job.get("type").equals("entry")  == entry) {
				String time = job.getString("time");
				String region = job.getString("region");
				eventList.add(new EventModel(subjectId , time, region));
			}
		}
	}

	public static String getJsonString() {
		String responseString = "Something went wrong fetching data from server.";
		try {
			InputStream response = new URL(baseUrl).openStream();
			try (Scanner scanner = new Scanner(response)) {
				responseString = scanner.useDelimiter("\\A").next();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return responseString;
	}
}
