package Controllers;

import Models.EdgeModel;
import Models.NodeModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public abstract class JsonFactory {

	public static String getJson(ArrayList<NodeModel> nodes, ArrayList<EdgeModel> edges) {
		StringBuilder sb = new StringBuilder("{\"nodes\":[");
		addNodesToJsonStringBuilder(sb, nodes);
		sb.append("],\"links\":[");
		addEdgesToJsonStringBuilder(sb, edges);
		sb.append("]}");

		return sb.toString();
	}

	private static void addNodesToJsonStringBuilder(StringBuilder sb, ArrayList<NodeModel> nodes) {
		Iterator nodeIter = nodes.iterator();
		while (nodeIter.hasNext()) {
			NodeModel node = (NodeModel) nodeIter.next();
			String name = node.getName();
			long time = node.getTime();
			sb.append("{\"name\":\"" + name +
					"\", \"time\":" + time + "}");
			if (nodeIter.hasNext()) {
				sb.append(", ");
			}
		}
	}

	private static void addEdgesToJsonStringBuilder(StringBuilder sb, ArrayList<EdgeModel> edges) {
		HashMap<String, String> cycleIdMap = getCycleIds(edges); //map cycle names to ids, because the names tend to get really long.
		Iterator edgeIter = edges.iterator();
		while (edgeIter.hasNext()) {
			EdgeModel em = (EdgeModel) edgeIter.next();
			String source = "\"" + em.getSource().getName() + "\"";
			String target = "\"" + em.getTarget().getName() + "\"";
			int weight = em.getWeight();
			int realVisitWeight = em.getRealVisitWeight();
			Long correlation = em.getTimeCorrelation();
			int defaultVisible = em.getDefaultVisible();
			double markovWeight = em.getMarkovWeight();
			sb.append("{\"source\":" + source
					+ ",\"target\":" + target
					+ ", \"weight\":" + weight
					+ ", \"realVisitWeight\":" + realVisitWeight
					+ ", \"markov_weight\":" + markovWeight
					+ ", \"correlation\":" + correlation
					+ ", \"defaultVisible\":" + defaultVisible
					+ ", \"tags\":[");
			ArrayList<String> tags = getCycleTags(em.getCycleWeightMap(), cycleIdMap);
			Iterator tagIter = tags.iterator();
			while (tagIter.hasNext()) {
				sb.append(tagIter.next());
				if (tagIter.hasNext()) {
					sb.append(", ");
				}
			}
			sb.append("]}");
			if (edgeIter.hasNext()) {
				sb.append(", ");
			}
		}
	}

	private static HashMap<String, String> getCycleIds(ArrayList<EdgeModel> edges) {
		HashMap<String, String> cycleIdMap = new HashMap<>();
		int id = 0;
		for (EdgeModel e : edges) {
			for (String s : e.getCycleWeightMap().keySet()) {
				if (!cycleIdMap.keySet().contains(s)) {
					cycleIdMap.put(s, "cycle " + id);
					id ++;
				}
			}
		}
		return cycleIdMap;
	}

	private static ArrayList<String> getCycleTags(HashMap<String, Integer> tagWeightMap, HashMap<String, String> cycleIdMap) {
		ArrayList<String> tags = new ArrayList<>();
		for (String name : tagWeightMap.keySet()) {
			tags.add("\"" + tagWeightMap.get(name) + ":" + name + "\"");
		}
		return tags;
	}
}
