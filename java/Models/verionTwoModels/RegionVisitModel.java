package Models.verionTwoModels;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import static UtilPackage.StaticValueArchive.PATH_END;

public class RegionVisitModel {

    private String regionName;
    private Date enterTime;
    private Date exitTime;
    private RegionVisitModel nextRegionVisited;
    private RegionVisitModel previousRegionVisited;
    private boolean isPathStartOrEnd;
    private boolean unreasonablyLengthy = false;
    private boolean repeatVisit = false;
    private boolean realVisit;

    public RegionVisitModel(String regionName) {
        isPathStartOrEnd = true;
        realVisit = false;
        this.regionName = regionName;
    }

    public RegionVisitModel(String regionName, Date enterTime, Date exitTime, RegionVisitModel previousRegionVisited) {
        isPathStartOrEnd = false;
        this.regionName = regionName;
        this.enterTime = enterTime;
        this.exitTime = exitTime;
        realVisit = true;
        this.previousRegionVisited = previousRegionVisited;
    }

    public void setNextRegionVisited(RegionVisitModel nextRegionVisited) {
        this.nextRegionVisited = nextRegionVisited;
    }

    public void setPreviousRegionVisited(RegionVisitModel previousRegionVisited) { this.previousRegionVisited = previousRegionVisited; }

    public String getRegionName() {
        return regionName;
    }

    public Date getEnterTime() {
        return enterTime;
    }

    public Date getExitTime() {
        return exitTime;
    }

    public Long getDuration() { return exitTime.getTime() - enterTime.getTime(); }

    public RegionVisitModel getNextRegionVisited() {
        return nextRegionVisited;
    }

    public RegionVisitModel getPreviousRegionVisited() {
        return previousRegionVisited;
    }

    public boolean hasNext() {
        return nextRegionVisited != null;
    }

    public void markAsUnreasonablyLengthy() {
        unreasonablyLengthy = true;
    }

    public void markAsNotRealVisit() { realVisit = false; }

    public void markAsRepeatVisit() { repeatVisit = true; }

    public boolean isRepeatVisit() { return repeatVisit; }

    public boolean isRealVisit() { return realVisit; }

    public boolean isUnreasonablyLengthy() {
        return unreasonablyLengthy;
    }

    public boolean isPathStartOrEnd() {
        return isPathStartOrEnd;
    }

    public void setExitTime(Date exitTime) {
        this.exitTime = exitTime;
    }
}
