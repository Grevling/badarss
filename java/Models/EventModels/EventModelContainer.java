package Models.EventModels;

import UtilPackage.Utils;
import java.util.*;

public class EventModelContainer {
    private HashMap<String, ArrayList<EventModel>> entryEventsPerSubject;
    private HashMap<String, ArrayList<EventModel>> exitEventsPerSubject;

    public EventModelContainer(HashMap<String, ArrayList<EventModel>> entryMap, HashMap<String, ArrayList<EventModel>> exitMap) {
        entryEventsPerSubject = entryMap;
        exitEventsPerSubject = exitMap;
        sortEventModels();
    }

    private void sortEventModels() {
        for (ArrayList<EventModel> list : entryEventsPerSubject.values()) {
            Collections.sort(list);
        }
        for (ArrayList<EventModel> list : exitEventsPerSubject.values()) {
            Collections.sort(list);
        }
    }

    public HashMap<String, ArrayList<EventModel>> getEntryEventsPerSubject() {
        return entryEventsPerSubject;
    }

    public HashMap<String, ArrayList<EventModel>> getExitEventsPerSubject() {
        return exitEventsPerSubject;
    }

}
