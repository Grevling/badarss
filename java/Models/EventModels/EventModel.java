package Models.EventModels;

import UtilPackage.Utils;
import java.util.Date;

public class EventModel implements Comparable<EventModel>{
    private EventModel nextEvent;
    private String subjectId;
    private Date time
    private String region; // Which beacon or collection of beacons triggered this event

    public EventModel(String subjectId, String time, String region) {
        this.subjectId = subjectId;
        this.time = Utils.dateFromString(time);
        this.region = region;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public boolean hasNext() {
        if (nextEvent != null) {
            return true;
        } else {
            return false;
        }
    }

    public EventModel getNextEvent() {
        return nextEvent;
    }

    public void setNextEvent(EventModel nextEvent) {
        this.nextEvent = nextEvent;
    }

    @Override
    public int compareTo(EventModel em) {
            long thisTime = time.getTime();
            long thatTime = em.getTime().getTime();
            if (thisTime > thatTime) {
                return 1;
            } else if (thisTime == thatTime) {
                return 0;
            } else {
                return -1;
            }
        }

}
