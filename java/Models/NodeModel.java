package Models;

import java.util.ArrayList;
import java.util.Map;
import static UtilPackage.Utils.millisToString;

public class NodeModel {

	private String name;
	private long time;
	private ArrayList<EdgeModel> outgoingEdgeList;

	public NodeModel(String name) {
		this.name = name;
		this.time = 0;
		outgoingEdgeList = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public void addTime(Long timeToAdd) { time = time + timeToAdd; }

	public long getTime() {
		return time;
	}

	public void addOutgoingEdge(EdgeModel edgeModel) {
		outgoingEdgeList.add(edgeModel);
	}

	public ArrayList<EdgeModel> getOutgoingEdgeList() {
		return outgoingEdgeList;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(name);
		sb.append("\n  Time: " + millisToString(time));
		sb.append("\n Out edges:");
		for (EdgeModel e : outgoingEdgeList) {
			sb.append("\n - " +e.toString());
		}
		return sb.toString();
	}
}
