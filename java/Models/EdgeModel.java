package Models;

import java.util.HashMap;

public class EdgeModel {
    private NodeModel source;
    private NodeModel target;
    private int weight;
    private int realVisitWeight = 0;
    private double markovWweight;
    private HashMap<String, Integer> cycleWeightMap;
    private boolean correlationEdge = false;
    private int defaultVisible;
    private Long timeCorrelation;

    public EdgeModel(NodeModel source, NodeModel target, int defaultVisible) {
        this.source = source;
        this.target = target;
        this.defaultVisible = defaultVisible;
        weight = 0;
        cycleWeightMap = new HashMap<>();
        source.addOutgoingEdge(this);
        timeCorrelation = new Long(0);
    }

    public void addCycleTag(String cycleId) {
        if (cycleWeightMap.keySet().contains(cycleId)) {
            cycleWeightMap.put(cycleId, cycleWeightMap.get(cycleId) + 1);
        } else {
            cycleWeightMap.put(cycleId, 1);
        }
    }

    public void markAsCorrelationEdge() { //marks this edge to be visible in correlation mode ( in tre d3.js visualisation web app)
        correlationEdge = true;
    }

    public HashMap<String, Integer> getCycleWeightMap() {
        return cycleWeightMap;
    }

    public NodeModel getSource() {
        return source;
    }

    public NodeModel getTarget() {
        return target;
    }

    public int getWeight() {
        return weight;
    }

    public int getRealVisitWeight() { return realVisitWeight; }

    public void addWeight() {
        defaultVisible = 1; weight++;
    }

    public void addRealVisitWeight() {
        realVisitWeight ++;
    }

    public double getMarkovWeight() {
        return markovWweight;
    }

    public void setMarkovWweight(double markovWweight) {
        this.markovWweight = markovWweight;
    }

    public void addTimeCorrelation(Long time){
        timeCorrelation += time;
    }

    public Long getTimeCorrelation() {
        return timeCorrelation;
    }

    public int getDefaultVisible() {
        return defaultVisible;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(target.getName());
        sb.append(" weight: " + weight + " mWweight: " + markovWweight);
        return sb.toString();
    }
}