
function getCycleNameArray(argArray) {
    var cycleNameArray = [];
    var index = 0;
    argArray.forEach(function (d) {
        cycleNameArray[index] = getCycleNameFromIdString(d);
        index++;
    });
    return cycleNameArray;
}

function registerAllCycles() {
    d3.selectAll('path.link')
        .each(function(d) {
            if (typeof d.tags != "undefined") {
                registerCycles(d.tags);
            }
        });
}

function registerCycles(cycleIdArray) {
    var length = cycleIdArray.length;
    for (var i = 0; i < length; i++) {
        var id = cycleIdArray[i]
        if (!contains(cycles, id)) {
            cycles[cycles.length] = id;
        }
    }
}

function normalizeCycleArray(cycleArray){
    var highestValue = 0;
    cycleArray.forEach(function(d) {
        var thisValue = getCycleWeightFromIdString(d);
        if (thisValue > highestValue) {
            highestValue = thisValue;
        }
    });
    var normalizer = highestValue/50;
    var index = 0;
    cycleArray.forEach(function(d) {
        var name = getCycleNameFromIdString(d);
        var originalValue = getCycleWeightFromIdString(d);
        var normalizedValue = Math.round(originalValue/normalizer);
        normalizedCycles[index] = [name, originalValue, normalizedValue, "cycle" + index];
        index ++;
    });
}

function getCycleNameFromIdString(id){
    return /(.*):(.*)/g.exec(id)[2];
}

function getCycleWeightFromIdString(id){
    var string = /(.*):(.*)/g.exec(id)[1];
    return parseInt(string);
}


function sortCycleArray() {
    normalizedCycles.sort(compareSecondColumn);
}