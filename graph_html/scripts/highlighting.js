

function removeManualHighlights() {
    removeManualNodeHighlights();
    removeManualEdgeHighlights();
}

function removeManualNodeHighlights() {
    var circle = d3.selectAll("circle");
    circle[0].forEach(function (d) {
        d.marked = false;
    });
    circle.transition()
        .duration(500)
        .style("fill", "#292929");
    manageMarkers();
}

function removeManualEdgeHighlights() {
    var links = d3.selectAll('path.link');
    links.classed("clicked", function (d) {
        d.clicked = false;
        return false;
    });
    manageMarkers();
}

function clearCycleHighlights(){
    highlightingCycleBool = false;
    deselectAllBarsInBarChart();
    d3.selectAll('path.link').classed("cycle", function (d) {
        d.cycle = false;
        return d.cycle;
    });
    d3.selectAll('path.link').classed("faded", false);
    manageMarkers();
}

function manageMarkers() {
    if (rawBool) {
        rawMarkers();
    } else if (markovBool) {
        markovMarkers();
    }else {
        removeMarkers();
    }
}

function rawMarkers() {
    d3.selectAll('path.link').attr("marker-end", function (d) {
        return "url(#end" + d.rawEnd + ")";
    });
    d3.selectAll('path.link.cycle').attr("marker-end", function (d) {
        return "url(#cycleEnd" + d.rawEnd + ")";
    });
    d3.selectAll('path.link.clicked').attr("marker-end", function (d) {
        return "url(#clickEnd" + d.rawEnd + ")";
    });

    d3.selectAll('path.link.clicked.cycle').attr("marker-end", function (d) {
        return "url(#clickcycleEnd" + d.rawEnd + ")";
    });
}

function markovMarkers() {
    d3.selectAll('path.link').attr("marker-end", function (d) {
        return "url(#end" + d.markovEnd + ")";
    });
    d3.selectAll('path.link.cycle').attr("marker-end", function (d) {
        return "url(#cycleEnd" + d.markovEnd + ")";
    });
    d3.selectAll('path.link.clicked').attr("marker-end", function (d) {
        return "url(#clickEnd" + d.markovEnd + ")";
    });

    d3.selectAll('path.link.clicked.cycle').attr("marker-end", function (d) {
        return "url(#clickcycleEnd" + d.markovEnd + ")";
    });
}

function removeMarkers() {
    d3.selectAll('path.link').attr("marker-end", null);
}

function highlightCycle(cycleId) { //TODO clicking the highlit bar should clear highlights
    highlightingCycleBool = true;
    deselectAllBarsInBarChart();
    document.getElementById("bar" + cycleId).className = "selectedBar";

    d3.selectAll('path.link').classed("cycle", function(d) {
        d.cycle = contains(getCycleNameArray(d.tags), cycleId.toString());

        return d.cycle;
    });

    manageMarkers();
}