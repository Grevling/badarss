
var transparencyBool = true;
var highlightingCycleBool = false;

var opacityClasses = ["twofive", "fivezero", "sevenfive", "onezerozero"];
var normalizedCycles = [];

var nodes = {};
var cycles = [];
var force;
var svg;

var defaultEdgeColour = "#005074";

var graph;

resetCheckedValues();



d3.json("graph.json", function(error, graphArg) {

    graph = graphArg;

    var nodeCount = 0;
    graph.nodes.forEach(function(node) {
        nodeCount++;

        nodes[node.name] = {name: node.name, time: node.time};
    });
    console.log("nodeCount1: " + nodeCount);

// Compute the distinct nodes from the links.
    graph.links.forEach(function(link) {
        link.source = nodes[link.source];
        link.target = nodes[link.target];
        link.weight = +link.weight;
    });

    var width = 1000,
        height = 700;

    force = d3.layout.force()
        .nodes(d3.values(nodes))
        .links(graph.links)
        .size([width, height])
        .linkDistance(250)
        .charge(-300)
        .on("tick", tick)
        .start();

    setScales();

	force.links().forEach(function (d) {
		d.clicked = false;
		d.cycle = false;
		d.weightedCorrelation = Math.round(linkCorrelationOneToTen(d.correlation));
		d.abosluteWidth = Math.round(linkWeightTwoToSix(d.weight));
		d.realVisitsWidth = Math.round(linkRealVisitWeightTwoToSix(d.realVisitWeight));
	});


    svg = d3.select(".svgDiv").append("svg")
        .attr("width", width)
        .attr("height", height);

    var maxArrowSize = 10;

    for(i = 0; i < maxArrowSize; i++) {
        buildArrows(i+1);
    }


// add the links and the arrows
    var linkNumber = 0;
    var path = svg.append("svg:g").selectAll("path")
        .data(force.links())
        .enter().append("svg:path")
        .attr("class", "link")
        .attr("stroke-width",  function (d) {
            return d.abosluteWidth})
        .attr("tags", function(d) { return  d.tags; })
        .attr("source" , function (d) { return d.source.name; })
        .attr("target" , function (d) { return d.target.name; })
        .classed("hidden", function (d) {
            d.defaultHidden = true;
            if(d.defaultVisible == 1){
                d.defaultHidden = false;
            }
            return d.defaultHidden;
        })
        .attr("id", function(d) {
            d.id = d.source.name+d.target.name;
            return d.id;
        })
        .on('click', function (d) { d.clicked = !d.clicked; pathClick();})
        .attr("marker-end", function (d) {
            d.markovEnd = Math.round(10 * d.markov_weight);

            d.rawEnd = Math.round(linkWeightTwoToSix(d.weight));
            return "url(#end" + d.rawEnd + ")";
        });


    // set opacityclasses to links
    var numberOfLinks = 0;
    var hiddenLinks = 0;
    graph.links.forEach(function(link) {
        setLinkRawOpacityClass(link);
        setLinkRealVisitOpacityClass(link)
        setLinkMarkovOpacityClass(link);
        setLinkCorrelationOpacityClass(link);

        if(link.defaultHidden == 1) {
            hiddenLinks ++;
        }
        numberOfLinks ++;
    });

    console.log("number of links: " + numberOfLinks);
    console.log("of those " + hiddenLinks + " are hidden.");

    force.links().forEach(function (d) {
        var className = "defaultHidden" + d.defaultHidden;
        if(!d.defaultHidden){
            svg.append("text")
                .attr("class", className)
                .attr("id", "curve-text")
                .append("textPath")
                .attr("xlink:href", function () {
                    return '#'+d.id;
                })
                .text(function () {
                    return "_ _ _  P: " + truncateMarkovValue(d.markov_weight);
                });
        }

    });

    updateMarkovLabelsVisibility();
    manageLinkTransparency();

    function pathClick() {
        d3.selectAll('path.link').classed("clicked", function(d) {
            return d.clicked;
        });
        manageMarkers();
    }

// define the nodes
    var node = svg.selectAll(".node")
        .data(force.nodes())
        .enter().append("g")
        .attr("class", "node")
        //			.on("click", click)
        .on("dblclick", dblclick)
        .call(force.drag);

// Set the range

// add the nodes
    node.append("circle")
        .attr("r", function (d) {
            var radius = scaledNodeWeight(d.time);//timeweight
            d.radius = radius;
            return radius;
        })
        .attr("fill", "#292929");

// add the text
    node.append("text")
        .attr("x", 12)
        .attr("dy", ".35em")
        .text(function(d) {
            return d.name; });

// add the curvy lines
    function tick() {
        path.attr("d", function(d) {
            var dx = d.target.x - d.source.x,
                dy = d.target.y - d.source.y,
                dr = Math.sqrt(dx * dx + dy * dy),
                offsetX = (dx * (d.target.radius)) / dr,
                offsetY = (dy * (d.target.radius)) / dr;
            
            if(dx == 0 && dy ==0 && dr ==0) {
                console.log("argh!" + d.id);
            }
            return "M" +
                d.source.x + "," +
                d.source.y + "A" +
                dr + "," + dr + " 0 0,1 " +
                (d.target.x - offsetX) +
                "," + (d.target.y - offsetY);
        });
        node.attr("transform", function(d) {
            return "translate(" + d.x + "," + d.y + ")"; });
    }

// action to take on mouse double click
    function dblclick() {
        var circle = d3.select(this).select("circle")[0][0]; //no idea what's going on here.

        if(circle.marked != true) {
            circle.marked = true;
            d3.select(this).select("circle").transition()
                .duration(750)
                .style("fill", "#9a0002");
        }else{
            circle.marked = false;
            d3.select(this).select("circle").transition()
                .duration(750)
                .style("fill", "#292929");
        }
    }
    makeBarGraph();
    setTimeout(fixNodes, 1500); //fix nodes after 1.5 seconds
});

function makeBarGraph() {
    registerAllCycles();
    normalizeCycleArray(cycles);
    sortCycleArray();

    var x = d3.scale.linear()
        .domain([0, 50])
        .range([0, 960]);

    d3.select(".chart")
        .selectAll("div")
        .data(normalizedCycles)
        .enter().append("div")
        .attr("id", function (d) { return "bar" + d[0]})
        .attr("title" , function(d) { return d[0]; })
        .style("width", function(d) { return x(d[2]) + "px"; })
        .text(function(d) { return d[3] + " : " + d[1]; })
        .on('click', function(d) {
            highlightCycle(d[0]);
        });
}

function deselectAllBarsInBarChart() {
    var selectedBars = document.getElementsByClassName("selectedBar");

    if (selectedBars.length == 1) {
        selectedBars[0].className = "notSelected";
    }
}


