function updateModeState() {
	realVisitsCheckEnabled(rawBool);
    manageEdges();
    manageLinkTransparency();
	manageMarkers();
	updateVisibility();
	updateMarkovLabelsVisibility();
}

function manageLinkTransparency () {
	if(transparencyChecked()) {
		if(markovModeChecked()) {
			enableMarkovLinkTransparency();
		} else if (correlationModeChecked()) {
			enableCorrelationLinkTransparency();
		} else if (realVisitChecked()){
			enableRealVisitTransparency();
		}else {
			enableRawLinkTransparency();
		}
	}else {
		disableLinkTransparency();
	}
}

function manageEdges() {
	if(markovModeChecked()) {
		markovEdges();
	} else if (correlationModeChecked()) {
		correlationEdges();
	} else if (realVisitChecked()){
		realVisitEdges();
	}else {
		absoluteEdges();
	}
}

function absoluteEdges() {
    d3.selectAll('path.link')
        .attr("stroke-width",  function (d) {
            return d.abosluteWidth;
        })
}

function realVisitEdges() {
    d3.selectAll('path.link')
        .attr("stroke-width",  function (d) {
            return d.realVisitsWidth;
        })
}

function markovEdges() {
    d3.selectAll('path.link')
        .attr("stroke-width",  function (d) {
            return 1 + (6 * d.markov_weight);
        })
}

function correlationEdges() {
    d3.selectAll('path.link')
        .attr("stroke-width",  function (d) {
            return d.weightedCorrelation;
        })
}

function updateMarkovLabelsVisibility() {
    svg.selectAll("#curve-text").classed('hidden', !markovBool);
}

function updateVisibility() {
    if (markovModeChecked()) {
        markovVisibility();
    } else if (correlationModeChecked()) {
        correlationVisibility();
    } else if (realVisitChecked()) {
        realVisitVisibility();
    } else {
        rawVisitility();
    }

}

function rawVisitility() {
    console.log('rawVisitility');
    d3.selectAll('path.link')
        .classed("hidden",  function (d) {
            return (d.defaultVisible == 0);
        })
}

function realVisitVisibility(){
    console.log('realVisitVisibility');
    d3.selectAll('path.link')
        .classed("hidden",  function (d) {
            return (d.realVisitWeight == 0);
        })
}

function correlationVisibility(){
    console.log('correlationVisibility');
    d3.selectAll('path.link')
        .classed("hidden",  function (d) {
            return (d.correlation == 0);
        })
}

function markovVisibility() {
    console.log('markovVisibility');
    d3.selectAll('path.link')
        .classed("hidden",  function (d) {
            return (d.defaultVisible == 0);
        })
}