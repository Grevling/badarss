
function compareSecondColumn(a, b) {
    if (a[1] === b[1]) {
        return 0;
    }
    else {
        return (a[1] > b[1]) ? -1 : 1;
    }
}

function contains(a, obj) {
    for (var i = 0; i < a.length; i++) {
        if (a[i] === obj) {
            return true;
        }
    }
    return false;
}

function truncateMarkovValue(markov) {
    var markovString = markov.toString();
    if(markovString.length > 9) {
        markovString = markovString.substr(0,7)+"...";
    }
    return markovString;
}

function realVisitsCheckEnabled(enabled) {
    d3.selectAll('#realVisitsCheck').classed("disabled", !enabled);
    document.getElementById("realVisitsCheck").disabled = !enabled;
}

function resetCheckedValues() {
    document.getElementById("tranSlider").checked = true;
    document.getElementById("fixSlider").checked = false;
    document.getElementById("rawRadio").checked = true;
}

function realVisitChecked() {
    return document.getElementById('realVisitsCheck').checked;
}

function transparencyChecked() {
    return document.getElementById("tranSlider").checked;
}

function markovModeChecked() {
    return document.getElementById("markovRadio").checked;
}

function correlationModeChecked() {
    return document.getElementById("correlationRadio").checked;
}

function fixNodes() {
    document.getElementById("fixSlider").checked = true;
    updateNodeFixing();
}

function updateNodeFixing() {
    var checked = document.getElementById("fixSlider").checked;
    d3.values(nodes).forEach(function (d) {
        d.fixed = checked;
    });
}

function disableLinkTransparency() {
    opacityClasses.forEach(function (p) {
        d3.selectAll('path.link').classed(p, false);
    });
}

function enableRawLinkTransparency() {
    opacityClasses.forEach(function (p) {
        d3.selectAll('path.link').classed(p, function (d) {
            return d.rawOpacityClass == p;
        });
    });
}

function enableRealVisitTransparency() {
    opacityClasses.forEach(function (p) {
        d3.selectAll('path.link').classed(p, function (d) {
            return d.realVisitOpacityClass == p;
        });
    });
}

function enableMarkovLinkTransparency() {
    opacityClasses.forEach(function (p) {
        d3.selectAll('path.link').classed(p, function (d) {
            return d.markovOpacityClass == p;
        });
    });
}

function enableCorrelationLinkTransparency() {
    opacityClasses.forEach(function (p) {
        d3.selectAll('path.link').classed(p, function (d) {
            return d.correlationOpacityClass == p;
        });
    });
}

var linkWeightTwoToSix;
var linkRealVisitWeightZeroToSix;
var linkRealVisitWeightTwoToSix;
var linkRealVisitWeightZeroToThree;
var linkCorrelationOneToTen;
var linkCorrelationZeroToThree;
var scaledNodeWeight;
function setScales() {
    var rawMaxWeight = d3.max(graph.links, function(d) { return d.weight; });
    linkWeightTwoToSix = d3.scale.linear().range([2, 6]).domain([0, rawMaxWeight]);

    var rawRealVisitMaxWeight = d3.max(graph.links, function(d) { return d.realVisitWeight; });
    linkRealVisitWeightZeroToSix = d3.scale.linear().range([0, 6]).domain([0, rawRealVisitMaxWeight]);
    linkRealVisitWeightZeroToThree = d3.scale.linear().range([0, 3]).domain([0, rawRealVisitMaxWeight]);
	linkRealVisitWeightTwoToSix  = d3.scale.linear().range([2, 6]).domain([0, rawRealVisitMaxWeight]);

    var correlationMax = d3.max(graph.links, function(d) { return d.correlation; });
    linkCorrelationZeroToThree = d3.scale.linear().range([0, 3]) .domain([0, correlationMax]);

    linkCorrelationOneToTen = d3.scale.linear().range([0, 10]).domain([0, correlationMax]);

    var max = d3.max(d3.values(nodes), function(d) { /*console.log("name: " + d.name + " time: " + d.time);*/ return d.time; });
    scaledNodeWeight = d3.scale.linear().range([5, 20]).domain([0, max]);

// Scale the range of the data
    console.log("raw node max is " + max);
    console.log("scaled node max is " + scaledNodeWeight(max));
    console.log("scaled node min is " + scaledNodeWeight(0));

    console.log("raw link max values: " + rawMaxWeight + " => " + linkWeightTwoToSix(rawMaxWeight));
    console.log("corrleation link max values: " + correlationMax + " => " + linkCorrelationOneToTen(correlationMax));
}

function buildArrows(size) {
    buildArrow(size, "end", defaultEdgeColour);
    buildArrow(size, "cycleEnd", "red");
    buildArrow(size, "clickEnd", "#5000ff");
    buildArrow(size, "clickcycleEnd", "#b600ca");
}

function buildArrow(size, type, colour) {
    var arrowSize = 4;
    var refX = 10.5 - (size * 0.5);
    svg.append("svg:defs").selectAll("marker")
        .data([type + size])      // Different link/path types can be defined here
        .enter().append("svg:marker")    // This section adds in the arrows
        .attr({
            "id": String,
            "viewBox":"0 -5 10 10",
            "refX":refX,
            "refY":-0,
            "markerWidth":arrowSize,
            "markerHeight":arrowSize,
            "orient":"auto"
        })
        .style("fill", colour)
        .append("svg:path")
        .attr("d", "M0,-5L10,0L0,5");
}

function setLinkRawOpacityClass(link){
    var linkValue = Math.floor(linkWeightTwoToSix(link.weight))-2;
    if(linkValue > 3) {
        linkValue = 3; // 3 is the highest allowed index of opacityClasses array
    }
    link.rawOpacityClass = opacityClasses[linkValue];
}

function setLinkRealVisitOpacityClass(link){
    var linkValue = Math.round(linkRealVisitWeightZeroToThree(link.realVisitWeight));
    if(linkValue > 3) {
        console.log("realVisit opacity thing exceeds 3");
        linkValue = 3; // 3 is the highest allowed index of opacityClasses array
    }
    link.realVisitOpacityClass = opacityClasses[linkValue];
}

function setLinkMarkovOpacityClass(link){
    var linkValue = Math.floor(3 * link.markov_weight);
    if(linkValue > 3) {
        console.log("markov opacity thing exceeds 3");
        linkValue = 3; // 3 is the highest allowed index of opacityClasses array
    }
    link.markovOpacityClass = opacityClasses[linkValue];

}

function setLinkCorrelationOpacityClass(link){
    var linkValue = Math.round(linkCorrelationZeroToThree(link.correlation));
    if(linkValue > 3) {
        console.log("correlation opacity thing exceeds 3 (utils.js)");
        linkValue = 3; // 3 is the highest allowed index of opacityClasses array
    }
    link.correlationOpacityClass = opacityClasses[linkValue];
}
